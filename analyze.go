package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

const (
	flagEntropyLevel  = "gitleaks-entropy-level"
	flagCommitFrom    = "commit-from"
	flagCommitTo      = "commit-to"
	flagCommits       = "commits"
	flagCommitsFile   = "commits-file"
	flagHistoricScan  = "full-scan"
	flagExcludedPaths = "excluded-paths"

	defaultPathGitleaksConfig = "/gitleaks.toml"
	defaultEntropy            = 3.7

	envVarCommitsFile = "SECRET_DETECTION_COMMITS_FILE"
	envVarCommits     = "SECRET_DETECTION_COMMITS"
	envVarEntropy     = "SECRET_DETECTION_ENTROPY_LEVEL"
	envVarCommitFrom  = "SECRET_DETECTION_COMMIT_FROM"
	envVarCommitTo    = "SECRET_DETECTION_COMMIT_TO"
	envVarFullScan    = "SECRET_DETECTION_HISTORIC_SCAN"

	maxSourceCodeLen          = 150
	leaksExitCode             = "0"
	gitleaksPassThroughTarget = "gitleaks.toml"
	entropyRuleTemplate       = `
[[rules]]
id = "generic-api-key"
description = "Generic API Key"
regex = '''(?i)((key|api|token|secret|password|pw)[a-z0-9_ .\-,]{0,25})(=|>|:=|\|\|:|<=|=>|:).{0,5}['\"]([0-9a-zA-Z\-_=]{8,64})['\"]'''
entropy = %f
secretGroup = 4
`
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.Float64Flag{
			Name:    flagEntropyLevel,
			Usage:   "Gitleaks entropy level (0.0 to 8.0)",
			EnvVars: []string{envVarEntropy},
			Value:   defaultEntropy,
		},
		&cli.StringFlag{
			Name:    flagCommitFrom,
			Usage:   "Run a scan on a range of commits starting at this commit",
			EnvVars: []string{envVarCommitFrom},
		},
		&cli.StringFlag{
			Name:    flagCommitTo,
			Usage:   "Run a scan on a range of commits stopping at this commit",
			EnvVars: []string{envVarCommitTo},
		},
		&cli.BoolFlag{
			Name:    flagHistoricScan,
			Usage:   "Runs an historic (all commits) scan on the repository",
			EnvVars: []string{envVarFullScan},
		},
		&cli.StringFlag{
			Name:    flagCommits,
			Usage:   "Commits is a list of comma separated commits for Gitleaks to scan",
			EnvVars: []string{envVarCommits},
		},
		&cli.StringFlag{
			Name:    flagCommitsFile,
			Usage:   "CommitsFile is a file containing a list of commits delimited by newlines for Gitleaks to scan",
			EnvVars: []string{envVarCommitsFile},
		},
	}
}

// analyze runs the tools and produces a report containing issues for each detected secret leak.
func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// Load custom config if available
	rulesetPath := filepath.Join(path, ruleset.PathSecretDetection)
	customRuleset, err := ruleset.Load(rulesetPath, "secrets")
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return nil, err
		}
	}

	// Create a temporary file. Gitleaks can't output to stdout.
	tmpFile, err := ioutil.TempFile("", "gitleaks-*.json")
	if err != nil {
		log.Errorf("Couldn't create temporary file: %v\n", err)
		return nil, err
	}

	pathGitleaksConfig, err := configPath(path, customRuleset)
	if err != nil {
		return nil, err
	}

	// add entropy rule if needed
	if c.Float64(flagEntropyLevel) != defaultEntropy {
		if err := addEntropyRule(c.Float64(flagEntropyLevel), pathGitleaksConfig); err != nil {
			return nil, err
		}
	}

	// default args will run a full history scan
	defaultArgs := []string{"detect", "--report-path", tmpFile.Name(),
		"--report-format", "json", "--source", path,
		"--config", pathGitleaksConfig, "--exit-code", leaksExitCode}
	args, err := appendScanTypeArgs(c, defaultArgs)
	if err != nil {
		return nil, err
	}
	cmd := exec.Command("gitleaks", args...)
	cmd.Env = os.Environ()

	// Listen for stderr coming from gitleaks
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, err
	}
	scannerStdErr := bufio.NewScanner(stderr)
	go func() {
		for scannerStdErr.Scan() {
			m := scannerStdErr.Text()
			log.Warn(m)
		}
	}()

	// Listen for stdout coming from gitleaks
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	scannerStdOut := bufio.NewScanner(stdout)
	go func() {
		for scannerStdOut.Scan() {
			m := scannerStdOut.Text()
			log.Info(m)
		}
	}()

	// start gitleaks command
	log.Debugf("Running gitleaks command: %s\n", cmd.String())
	if err := cmd.Start(); err != nil {
		return nil, err
	}

	// wait for gitleaks command git finish
	if err := cmd.Wait(); err != nil {
		return nil, err
	}

	return os.Open(tmpFile.Name())
}

// appendScanTypeArgs accepts a cli.Context pointer, and a slice of strings representing default gitleaks arguments and
// determines what type of scan gitleaks should run. Possible options:
// --no-git: runs gitleaks on the files of a repo. This scan does not iterate through commit histories
// --commits: scans a range of commits specified in the analyzer cli arguments
// --commits-file: scans a range of commits specified in commitFiles
// --from-commit and --to-commit: scans a range of commits based on --from-commit and --to-commit
// --historic: scans the full git history
func appendScanTypeArgs(c *cli.Context, args []string) ([]string, error) {
	if c.String(flagCommits) != "" {
		commits := strings.Split(c.String(flagCommits), ",")
		return append(args, []string{"--log-opts", fmt.Sprintf("--no-merges %s^..%s", commits[len(commits)-1], commits[0])}...), nil
	}
	if c.String(flagCommitsFile) != "" {
		content, err := ioutil.ReadFile(c.String(flagCommitsFile))
		if err != nil {
			return nil, err
		}
		commits := strings.Split(strings.Trim(string(content), "\n"), "\n")
		return append(args, []string{"--log-opts", fmt.Sprintf("--no-merges %s^..%s", commits[len(commits)-1], commits[0])}...), nil
	}
	if c.String(flagCommitFrom) != "" && c.String(flagCommitTo) != "" {
		return append(args, []string{"--log-opts", fmt.Sprintf("--no-merges %s^..%s", c.String(flagCommitFrom), c.String(flagCommitTo))}...), nil
	}
	if c.Bool(flagHistoricScan) {
		return args, nil
	}

	return append(args, "--no-git"), nil
}

func addEntropyRule(entropy float64, pathGitleaksConfig string) error {
	// append regex rule to the gitleaks config to maintain old functionality
	rule := fmt.Sprintf(entropyRuleTemplate, entropy)
	f, err := os.OpenFile(filepath.Clean(pathGitleaksConfig), os.O_APPEND|os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer safeClose(f)
	if _, err := f.WriteString(rule); err != nil {
		return err
	}
	return nil
}

// configPath will look at rulesets to determine the path for the gitleaks.toml
func configPath(projectPath string, customRuleset *ruleset.Config) (string, error) {
	// Set path to default
	pathGitleaksConfig := defaultPathGitleaksConfig

	if customRuleset == nil {
		return pathGitleaksConfig, nil
	}

	for _, passThrough := range customRuleset.PassThrough {
		if passThrough.Target == gitleaksPassThroughTarget {
			switch passThrough.Type {
			case ruleset.PassThroughFile:
				pathGitleaksConfig = filepath.Join(projectPath, passThrough.Value)
			case ruleset.PassThroughRaw:
				content := []byte(passThrough.Value)
				tmpfile, err := ioutil.TempFile("", "gitleaks*.toml")
				if err != nil {
					return "", err
				}

				log.Debugf("Gitleaks config: %s", passThrough.Value)
				if _, err := tmpfile.Write(content); err != nil {
					return "", err
				}

				if err := tmpfile.Close(); err != nil {
					return "", err
				}

				pathGitleaksConfig = tmpfile.Name()
				log.Debugf("Gitleaks config path: %s", pathGitleaksConfig)
			}
		}
	}

	return pathGitleaksConfig, nil
}

func safeClose(c io.Closer) {
	if err := c.Close(); err != nil {
		log.Warn(err)
	}
}
